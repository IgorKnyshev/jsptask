package by.training.tags;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class ResolveUrl extends SimpleTagSupport {
	private String url;

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public void doTag() {
		PageContext context = (PageContext) getJspContext();
		JspWriter out =  context.getOut();
		try {
			out.println("<h3> Real path of component: </h3>");
			out.println(url + " - " + ((PageContext) getJspContext()).getServletContext().getRealPath(url));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

package by.training.tags;

import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;
import java.util.Map;

public class ListMapping extends BodyTagSupport {

	@Override
	public int doEndTag() {
		BodyContent body = getBodyContent();
		JspWriter out = body.getEnclosingWriter();
		try {
			out.println("<h3> List of all mappings: </h3>");
			for (Map.Entry<String, ? extends ServletRegistration> result : pageContext.getServletContext().getServletRegistrations().entrySet()) {
				out.println("<p>" + ((ServletRegistration)result.getValue()).getMappings() + " - " + result.getKey());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return EVAL_PAGE;
	}

}
